
## JS Quest Weather

This project is built for the purpose of solving the JS Quest weather app demo assignment.
The following features are included in the latest version

### Shows the user weather information by default without searching
It uses the Geolocation API to get the user location and shows the current weather of the user's city<br />
![](/gif/loading.gif)
<br />
### Shows weather info of a searched city
### The application theme changes dynamically with the temperature of the weather
its uses the colors of Blue to show from cold to medium temperature
it uses the orange grey color families to show a temperature between hot and cold
it uses the Red colors to show hot temperatures. There are variets of 45 colors to represent the remperature levels<br />
To see this in action search for the following cities and see the application themes change<br /><br />
-- Bangkok and Lagos for hot temperature range cities<br />
![](/gif/hot.gif)
-- Addis Ababa for medium range cities<br />
-- Prague,Moscow,Chicago for cold range cities<br />
![](/gif/cold.gif)

### Running the application
-- `node index.js` to start the node.js express server
-- `npm start` to start the create-react-app

