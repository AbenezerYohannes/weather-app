import { UserLocationAction } from "./../actions/container";
import { ContainerState } from "./../types/Container";
import * as constants from "./../constants/container";

export const initialContainerState = {
  containerBackgroundColor: "#E0E0E0",
  colorsList: [
    "#FFFFFF",
    "#E1F5FE",
    "#B3E5FC",
    "#81D4FA",
    "#4FC3F7",
    "#29B6F6",
    "#03A9F4",
    "#039BE5",
    "#0288D1",
    "#0277BD",
    "#01579B",
    "#4DD0E1",
    "#26C6DA",
    "#00BCD4",
    "#00ACC1",
    "#E6EE9C",
    "#DCE775",
    "#D4E157",
    "#CDDC39",
    "#F0F4C3",
    "#E6EE9C",
    "#DCE775",
    "#D4E157",
    "#CDDC39",
    "#FFE0B2",
    "#FFCC80",
    "#FFB74D",
    "#FFA726",
    "#FF9800",
    "#FB8C00",
    "#F57C00",
    "#EF9A9A",
    "#E57373",
    "#EF5350",
    "#F44336",
    "#E53935",
    "#D32F2F",
    "#C62828",
    "#B71C1C"
  ]
};

export default function(
  state: ContainerState = initialContainerState,
  action: UserLocationAction
): ContainerState {
  switch (action.type) {
    case constants.USER_LOCATION_REQUESTED:
      return { ...state };
    case constants.USER_LOCATION_REQUEST_SUCCESS:
      return { ...state };
    case constants.USER_LOCATION_REQUEST_ERROR:
      return { ...state };
    case constants.CITY_REQUESTED:
      return { ...state };
    case constants.CITY_REQUEST_SUCCESS:
      return {
        ...state,
        selectedCity: action.payload
      };
    case constants.WEATHER_REQUESTED:
      return { ...state };
    case constants.WEATHER_REQUEST_SUCCESS:
      return {
        ...state,
        weatherInformation: action.payload
      };
    case constants.WEATHER_REQUEST_ERROR:
      return { ...state };
    case constants.CITY_REQUEST_ERROR:
    default:
      return state;
  }
}
