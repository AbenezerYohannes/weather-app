import { SearchCityAction } from "./../actions/searchCity";
import { SearchCityState } from "../types/SearchCity";
import * as constants from "./../constants/searchCity";

const initialSearchState = {
  searchQuery: "",
  cityList: []
};

export default function(
  state: SearchCityState = initialSearchState,
  action: SearchCityAction
): SearchCityState {
  console.log("action: ", action);
  switch (action.type) {
    case constants.SEARCH_CITY_REQUESTED:
      return { ...state };
    case constants.SEARCH_CITY_REQUEST_SUCCESS:
      return { ...state, cityList: action.payload };
    case constants.SEARCH_CITY_REQUEST_ERROR:
      return { ...state };
    default:
      return { ...state };
  }
}
