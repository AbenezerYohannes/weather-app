import React from "react";
import styled from "styled-components";

interface RowProps {
  marginBottom?: string;
}

const Row = styled.div<RowProps>`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin-bottom: ${props => props.marginBottom};
`;

interface RowItemProps {
  width: string;
  height: string;
  marginTop?: string;
  borderRadius?: string;
}

const FirstRowItem = styled.div<RowItemProps>`
  width: ${props => props.width};
  height: ${props => props.height};
  margin-top: ${props => props.marginTop};
  border-radius: ${props => props.borderRadius};
  background: #eaebed;
  background-image: linear-gradient(
    to right,
    #eaebed 0%,
    #edeef1 40%,
    #f6f7f8 80%,
    #f6f7f8 100%
  );
  background-repeat: no-repeat;
  background-size: 800px 104px;
  animation-duration: 1s;
  animation-fill-mode: forwards;
  animation-iteration-count: infinite;
  animation-name: placeholderShimmer;
  animation-timing-function: linear;
  @keyframes placeholderShimmer {
    0% {
      background-position: -468px 0;
    }

    100% {
      background-position: 468px 0;
    }
  }
`;
export default function LoadingIndicator() {
  return (
    <div>
      <Row marginBottom="30px">
        <div>
          <FirstRowItem width="150px" height="70px" borderRadius="7px" />
          <FirstRowItem width="150px" height="20px" marginTop="7px" />
        </div>
        <div>
          <FirstRowItem width="150px" height="70px" borderRadius="7px" />
          <FirstRowItem width="70px" height="20px" marginTop="7px" />
        </div>
        <div>
          <FirstRowItem width="150px" height="70px" borderRadius="7px" />
        </div>
      </Row>
      <Row marginBottom="0px">
        <div>
          <FirstRowItem width="130px" height="130px" borderRadius="75px" />
          <FirstRowItem width="130px" height="20px" marginTop="7px" />
        </div>
        <div>
          <FirstRowItem width="130px" height="130px" borderRadius="75px" />
          <FirstRowItem width="130px" height="20px" marginTop="7px" />
        </div>
        <div>
          <FirstRowItem width="130px" height="130px" borderRadius="75px" />
          <FirstRowItem width="130px" height="20px" marginTop="7px" />
        </div>
      </Row>
    </div>
  );
}
