import React from "react";
import styled from "styled-components";

const WeatherInfoContainer = styled.div`
  margin-top: 5px;
  display: flex;
  position: relative;
  flex-direction: column;
  align-items: center;
  width: 200px;
`;

interface WeatherInfoProps {
  rotate: string;
}

const WeatherInfoIcon = styled.div<WeatherInfoProps>`
  font-size: 90px;
  transition: all 0.3s ease-out;
  width: 110px;
  height: 110px;
  display: block;
  position: relative;
  transform: ${props => (props.rotate ? `rotate(${props.rotate}deg)` : "")};
  color: #dddcdc;
`;

const Label = styled.div`
  padding-top: 15px;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

interface Colorizer {
  iconColor: string;
}
const Result = styled.div<Colorizer>`
  font-size: 20px;
  color: ${props => props.iconColor};
  margin-top: 5px;
`;

const Icon = styled.i<Colorizer>`
  color: ${props => props.iconColor};
`;
export interface Props {
  windDirection?: string;
  windSpeed?: string;
  humidity?: string;
  iconClassName: string;
  label: string;
  iconColor: string;
}

export default function WeatherInfoWithIcon({
  windDirection,
  iconClassName,
  label,
  iconColor,
  windSpeed,
  humidity
}: Props) {
  return (
    <WeatherInfoContainer>
      <WeatherInfoIcon rotate={windDirection || "0"}>
        <Icon className={iconClassName} iconColor={iconColor} />
      </WeatherInfoIcon>
      <Label>
        <div>{label}</div>
        <Result iconColor={iconColor}>
          {Number.parseFloat(
            windDirection || windSpeed || humidity || "0"
          ).toFixed(2)}
        </Result>
      </Label>
    </WeatherInfoContainer>
  );
}
