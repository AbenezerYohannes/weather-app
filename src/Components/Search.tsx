import React, { useState } from "react";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "redux";
import * as actions from "../actions/searchCity";
import SearchResultList from "./SearchResultList";

const SearchContainer = styled.div`
  margin-bottom: 50px;
  position: relative;
`;

const SearchInput = styled.input`
  width: 705px;
  border: none;
  box-shadow: 0 2px 3px rgba(0, 0, 0, 0.06), 0 2px 3px rgba(0, 0, 0, 0.12);
  font-size: 18px;
  padding: 12px 0px 12px 20px;
  border-radius: 5px;
  outline: none;
  transition: 0.5s all ease;
  &:hover {
    box-shadow: 0 6px 10px rgba(0, 0, 0, 0.06), 0 6px 10px rgba(0, 0, 0, 0.12);
  }
  &:focus {
    box-shadow: 0 6px 10px rgba(0, 0, 0, 0.06), 0 6px 10px rgba(0, 0, 0, 0.12);
  }
`;
export default function Search() {
  const [searchInput, setSearchInput] = useState("");
  const [showSearchList, setShowSearchList] = useState(true);
  const searchCity = useSelector((state: any) => state.searchCity);
  const dispatch = useDispatch<Dispatch>();
  let onSearchInputChange = function(event: any) {
    setSearchInput(event.target.value);
    dispatch(actions.getCityList(event.target.value));
  };
  let onShowSearchList = () => {
    setShowSearchList(true);
  };
  let onHideSearchList = () => {
    setTimeout(() => {
      setShowSearchList(false);
    }, 500);
  };
  return (
    <SearchContainer>
      <SearchInput
        type="text"
        placeholder="Search for cities"
        value={searchInput}
        onChange={event => onSearchInputChange(event)}
        onFocus={onShowSearchList}
        onBlur={onHideSearchList}
      />
      {showSearchList && <SearchResultList cities={searchCity.cityList} />}
    </SearchContainer>
  );
}
