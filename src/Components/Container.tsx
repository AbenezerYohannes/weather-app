import React, { useEffect } from "react";
import styled from "styled-components";
import { useSelector, useDispatch } from "react-redux";
import { Dispatch } from "redux";
import * as actions from "../actions/container";
import WeatherInfo from "./WeatherInfo";
import WeatherInfoWithIcon from "./WeatherInfoWithIcon";
import LoadingIndicator from "./LoadingIndicator";
import Search from "./Search";
import logoSVG from "./../resources/logo.svg";
interface ContainerProps {
  containerBackground: string;
}

const Container = styled.div<ContainerProps>`
  background-color: ${props => props.containerBackground};
  padding: 0px;
  margin: 0px;
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  height: 100vh;
  font-family: Roboto;
`;

const MenuBar = styled.div`
  width: 100%;
  height: 45px;
  background-color: #ffffff;
  position: fixed;
  top: 0;
  box-shadow: 0 2px 3px rgba(0, 0, 0, 0.06), 0 2px 3px rgba(0, 0, 0, 0.12);
  display: flex;
  align-items: center;
  padding: 0px 0px 0px 40px;
`;

const WeatherResult = styled.div`
  width: 700px;
  height: auto;
  background-color: #ffffff;
  border-radius: 7px;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.06), 0 3px 6px rgba(0, 0, 0, 0.12);
  padding: 15px;
  z-index: 10;
`;

const WeatherResultsListContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding-bottom: 20px;
`;

const LogoSVG = styled.img`
  width: 35px;
  height: 35px;
`;

const Title = styled.div`
  font-size: 16px;
  color: #757575;
  padding-left: 10px;
`;

const FirstBackgroundLayer = styled.div`
  position: fixed;
  width: 120vw;
  height: 120vh;
  background: rgba(0, 0, 0, 0.05);
  transform: rotate(150deg);
  margin-top: 50vh;
  margin-left: 50vw;
`;
const SecondBackgroundLayer = styled.div`
  position: fixed;
  width: 120vw;
  height: 120vh;
  background: rgba(0, 0, 0, 0.05);
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
  margin-top: 120vh;
  margin-right: -100vw;
`;

export interface Props {
  containerBackgroundColor?: string;
  userLocationRequested?: () => void;
}

export default function() {
  const container = useSelector((state: any) => state.container);
  const dispatch = useDispatch<Dispatch>();
  useEffect(() => {
    dispatch(actions.getUserLocationWeather());
  }, []);
  let temperatureResult;

  function lightenDarkenColor(col: string, amt: number) {
    var usePound = false;
    if (col[0] == "#") {
      col = col.slice(1);
      usePound = true;
    }
    var num = parseInt(col, 16);
    var r = (num >> 16) + amt;
    if (r > 255) r = 255;
    else if (r < 0) r = 0;
    var b = ((num >> 8) & 0x00ff) + amt;
    if (b > 255) b = 255;
    else if (b < 0) b = 0;
    var g = (num & 0x0000ff) + amt;
    if (g > 255) g = 255;
    else if (g < 0) g = 0;
    return (usePound ? "#" : "") + (g | (b << 8) | (r << 16)).toString(16);
  }

  if (container.weatherInformation) {
    console.log("container.weatherInformation: ", container.weatherInformation);
    temperatureResult = (
      <WeatherResult>
        <WeatherInfo
          temperature={container.weatherInformation.the_temp}
          imageName={container.weatherInformation.weather_state_abbr}
          cityName={container.selectedCity.title}
          weather_state_name={container.weatherInformation.weather_state_name}
          air_pressure={container.weatherInformation.air_pressure}
        />
        <WeatherResultsListContainer>
          <WeatherInfoWithIcon
            label="Wind Direction"
            iconColor={lightenDarkenColor(
              container.colorsList[
                parseInt(container.weatherInformation.the_temp)
              ],
              -50
            )}
            windDirection={container.weatherInformation.wind_direction}
            iconClassName="icon-arrow"
          />
          <WeatherInfoWithIcon
            label="Wind Speed"
            iconColor={lightenDarkenColor(
              container.colorsList[
                parseInt(container.weatherInformation.the_temp)
              ],
              -50
            )}
            windSpeed={container.weatherInformation.wind_speed}
            iconClassName="icon-wind-speed"
          />
          <WeatherInfoWithIcon
            label="Humidity"
            iconColor={lightenDarkenColor(
              container.colorsList[
                parseInt(container.weatherInformation.the_temp)
              ],
              -50
            )}
            humidity={container.weatherInformation.humidity}
            iconClassName="icon-humidity"
          />
        </WeatherResultsListContainer>
      </WeatherResult>
    );
  } else {
    console.log("no weather information");
    temperatureResult = (
      <WeatherResult>
        <LoadingIndicator />
      </WeatherResult>
    );
  }
  return (
    <Container
      containerBackground={
        container.weatherInformation
          ? container.colorsList[
              parseInt(container.weatherInformation.the_temp)
            ]
          : "#f6f7f8"
      }
    >
      <FirstBackgroundLayer />
      <SecondBackgroundLayer />
      <MenuBar>
        <LogoSVG src={logoSVG} />
        <Title>JS Quest Weather</Title>
      </MenuBar>
      <Search />
      {temperatureResult}
    </Container>
  );
}
