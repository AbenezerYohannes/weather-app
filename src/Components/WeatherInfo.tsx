import React from "react";
import styled from "styled-components";

const WeatherImage = styled.img`
  width: 50px;
`;

const WeatherAndCitySeparator = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: flex-start;
`;

const WeatherImageAndTextContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  font-weight: 100;
`;

const TemperatureValue = styled.div`
  font-size: 30px;
  padding: 0px 5px;
  color: #212121;
`;

const WeatherStateText = styled.div`
  font-size: 16px;
  margin-top: -7px;
  color: #757575;
`;

const AirPressureContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Result = styled.div`
    font-size: 20px;
    color: #004d40;
    margin-top: 0px;
`;
export interface Props {
  temperature: string;
  imageName: string;
  cityName: string;
  weather_state_name: string;
  air_pressure: string;
}

export default function WeatherInfo({
  temperature,
  imageName,
  cityName,
  weather_state_name,
  air_pressure
}: Props) {
  return (
    <WeatherAndCitySeparator>
      <WeatherImageAndTextContainer>
        <WeatherImage
          src={`https://www.metaweather.com/static/img/weather/png/${imageName}.png`}
        />
        <br />
        <TemperatureValue>
          <div>
            {Number.parseFloat(temperature).toFixed(2)}
            <sup> o</sup>C
          </div>
          <WeatherStateText>{weather_state_name}</WeatherStateText>
        </TemperatureValue>
      </WeatherImageAndTextContainer>
      <AirPressureContainer>
        <TemperatureValue>Air pressure</TemperatureValue>
        <Result>{air_pressure}</Result>
      </AirPressureContainer>
      <TemperatureValue>{cityName}</TemperatureValue>
    </WeatherAndCitySeparator>
  );
}
