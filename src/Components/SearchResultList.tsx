import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { CityList } from "../types/SearchCity";
import { getWeatherInfo } from './../actions/container';
import { useDispatch } from 'react-redux';
import { Dispatch } from 'redux';
const SearchUL = styled.ul`
  background-color: #ffffff;
  position: absolute;
  top: 40px;
  width: 100%;
  border-radius: 5px;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.06), 0 3px 6px rgba(0, 0, 0, 0.12);
  list-style-type: none;
  padding: 0;
  z-index: 9999;
`;

const SearchListItem = styled.li`
  border-bottom: 1px solid #f6f7f8;
  padding: 10px 2px 10px 20px;
  color: #616161;
  :hover {
    background-color: #f6f7f8;
    cursor: pointer;
  }
`;

export interface Props {
  cities: CityList[];
}

export default function SearchResultList({ cities }: Props) {
  const dispatch = useDispatch<Dispatch>();
  let onSearchListItemClick = function(city: CityList) {
    dispatch(getWeatherInfo(city));
  };
  return (
    <SearchUL>
      {cities.map(city => (
        <SearchListItem
          key={city.woeid}
          onClick={() => onSearchListItemClick(city)}
        >
          {city.title}
        </SearchListItem>
      ))}
    </SearchUL>
  );
}
