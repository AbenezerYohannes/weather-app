export interface CityList {
  title: string;
  location_type: string;
  woeid: number;
  latt_long: string;
}

export interface SearchCityState {
  searchQuery: string;
  cityList?: CityList [];
}
