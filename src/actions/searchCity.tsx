import * as constants from "./../constants/searchCity";
import { Dispatch } from "redux";
import axios from "axios";
import { CityList } from '../types/SearchCity';

export interface SearchCityRequested {
  type: constants.SEARCH_CITY_REQUESTED;
}

export interface SearchCityRequestSuccess {
  type: constants.SEARCH_CITY_REQUEST_SUCCESS;
  payload: CityList [];
}

export interface SearchCityRequestError {
  type: constants.SEARCH_CITY_REQUEST_ERROR;
}

export type SearchCityAction =
  | SearchCityRequested
  | SearchCityRequestSuccess
  | SearchCityRequestError;

export function searchCityRequested(): SearchCityRequested {
  return {
    type: constants.SEARCH_CITY_REQUESTED
  };
}

export function searchCityRequestSuccess(payload: CityList []): SearchCityRequestSuccess {
  return {
    type: constants.SEARCH_CITY_REQUEST_SUCCESS,
    payload
  };
}
export function searchCityRequestError(): SearchCityRequestError {
  return {
    type: constants.SEARCH_CITY_REQUEST_ERROR
  };
}

export function getCityList(searchQuery: string): any {
  return async (dispatch: Dispatch) => {
    console.log("get city list was called");
    dispatch(searchCityRequested());
    let cityList: any = await axios.get(
      `/cities/${searchQuery}`
    );
    dispatch(searchCityRequestSuccess(cityList.data.slice(0, 5)));
    console.log('cityList.data: ', cityList.data);
    console.log("cityList: ", cityList);
  };
}
