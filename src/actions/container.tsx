import * as constants from "./../constants/container";
import { Dispatch } from "redux";
import axios from "axios";
import { WeatherInformation, CityInformation } from "./../types/Container";
import { CityList } from './../types/SearchCity';
// move this interface on the types interface

export interface UserLocationRequested {
  type: constants.USER_LOCATION_REQUESTED;
}

export interface UserLocationRequestSuccess {
  type: constants.USER_LOCATION_REQUEST_SUCCESS;
}

export interface UserLocationRequestError {
  type: constants.USER_LOCATION_REQUEST_ERROR;
}

export interface CityRequested {
  type: constants.CITY_REQUESTED;
}

export interface CityRequestSuccess {
  type: constants.CITY_REQUEST_SUCCESS;
  payload?: CityInformation;
}

export interface CityRequestError {
  type: constants.CITY_REQUEST_ERROR;
}

export interface WeatherRequested {
  type: constants.WEATHER_REQUESTED;
}

export interface WeatherRequestSuccess {
  type: constants.WEATHER_REQUEST_SUCCESS;
  payload?: WeatherInformation;
}

export interface WeatherRequestError {
  type: constants.WEATHER_REQUEST_ERROR;
}

export type UserLocationAction =
  | UserLocationRequested
  | UserLocationRequestSuccess
  | UserLocationRequestError
  | CityRequested
  | CityRequestSuccess
  | CityRequestError
  | WeatherRequested
  | WeatherRequestSuccess
  | WeatherRequestError;

export function userLocationRequested(): UserLocationRequested {
  return {
    type: constants.USER_LOCATION_REQUESTED
  };
}

export function userLocationRequestSuccess(): UserLocationRequestSuccess {
  return {
    type: constants.USER_LOCATION_REQUEST_SUCCESS
  };
}

export function userLocationRequestError(): UserLocationRequestError {
  return {
    type: constants.USER_LOCATION_REQUEST_ERROR
  };
}

export function cityRequested(): CityRequested {
  return {
    type: constants.CITY_REQUESTED
  };
}

export function cityRequestSuccess(
  payload: CityInformation
): CityRequestSuccess {
  return {
    type: constants.CITY_REQUEST_SUCCESS,
    payload
  };
}

export function cityRequestError(): CityRequestError {
  return {
    type: constants.CITY_REQUEST_ERROR
  };
}

//

export function weatherRequested(): WeatherRequested {
  return {
    type: constants.WEATHER_REQUESTED
  };
}

export function weatherRequestSuccess(
  payload: WeatherInformation
): WeatherRequestSuccess {
  return {
    type: constants.WEATHER_REQUEST_SUCCESS,
    payload
  };
}

export function weatherRequestError(): WeatherRequestError {
  return {
    type: constants.WEATHER_REQUEST_ERROR
  };
}

export function getUserLocationWeather(): any {
  return (dispatch: Dispatch) => {
    dispatch(userLocationRequested());
    if ("geolocation" in navigator) {
      /* geolocation is available */
      navigator.geolocation.getCurrentPosition(async function(position) {
        let { latitude, longitude } = position.coords;
        try {
          let cityInformation: any = await axios.get(
            `/location/search/${latitude}/${longitude}`
          );
          let weatherInformation: any = await axios.get(
            `/weather/${cityInformation.data[0].woeid}/`
          );
          dispatch(cityRequestSuccess(cityInformation.data[0]));
          dispatch(
            weatherRequestSuccess(
              weatherInformation.data.consolidated_weather[0]
            )
          );
        } catch (error) {
          console.log("error: ", error);
        }
      });
    } else {
      /* geolocation IS NOT available */
      console.log("geolocation IS NOT available");
    }
  };
}

export function getWeatherInfo (city: CityList): any {
  return async (dispatch: Dispatch) => {
    dispatch(cityRequestSuccess(city));
    try {
      let weatherInformation: any = await axios.get(
        `/weather/${city.woeid}/`
      );
      dispatch(
        weatherRequestSuccess(
          weatherInformation.data.consolidated_weather[0]
        ))
    } catch (error) {
      dispatch(weatherRequestError());
    }
  }
}
