const express = require("express");
const path = require("path");
const axios = require("axios");
const app = express();

app.use(express.static(path.join(__dirname, "build")));

app.get("/location/search/:lat/:long", async (req, res) => {
  try {
    let responseData = await axios.get(
      `https://www.metaweather.com/api/location/search/?lattlong=${req.params.lat},${req.params.long}`
    );
    return res.json(responseData.data);
  } catch (error) {
    console.log("error: ", error);
  }
});

app.get("/weather/:woeid", async (req, res) => {
  try {
    let responseData = await axios.get(
      `https://www.metaweather.com/api/location/${req.params.woeid}/`
    );
    return res.json(responseData.data);
  } catch (error) {
    console.log("error: ", error);
  }
});

app.get("/cities/:searchQuery", async (req, res) => {
  try {
    let responseData = await axios.get(
      `https://www.metaweather.com/api/location/search/?query=${req.params.searchQuery}`
    );
    return res.json(responseData.data);
  } catch (error) {
    console.log("error: ", error);
  }
});

app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "build", "index.html"));
});

app.listen(8080, () => {
  console.log("listening on port 8080");
});
